
/*Nama : purwana rijki pirmansah
 * sabtu 21 oktober 2023
*/
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        int panjang, lebar, tinggi, volume;
        System.out.print("masukan panjang balok anda\t: ");
        panjang = input.nextInt();// memasukan input panjang balok anda
        System.out.print("masukan lebar balok anda\t: ");
        lebar = input.nextInt();// memasukan input lebar balok anda
        System.out.print("masukan tinggi balok anda\t: ");
        tinggi = input.nextInt();// memasukan input tinggi balok anda
        volume = panjang * lebar * tinggi;// merumuskan volume balok anda
        System.out.println(panjang + " x " + lebar + " x " + tinggi + " = " + volume);
        input.close();// menampilkan hasilnya
    }
}
