import java.util.Scanner;

public class kasir {
    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        String[] buah = { "Mangga", "kelapa", "anggur", "pepaya", "jeruk", "salak", "buah naga" };
        int[] harga = { 5000, 6000, 10000, 8000, 3000, 2000, 4000 };
        int[] jumlah = new int[buah.length];
        int jumlahTotal = 0;

        System.out.println("\t Toko buah Purwana");
        System.out.println("|===================================|");
        System.out.println("| No |\tnama buah  |\t Harga      |");
        System.out.println("| 1. |\t mangga    |\t Rp.5000    |");
        System.out.println("| 2. |\t kelapa    |\t Rp.6000    |");
        System.out.println("| 3. |\t anggur    |\t Rp.10000   |");
        System.out.println("| 4. |\t pepaya    |\t Rp.8000    |");
        System.out.println("| 5. |\t jeruk     |\t Rp.3000    |");
        System.out.println("| 6. |\t salak     |\t Rp.2000    |");
        System.out.println("| 7. |\t buah naga |\t Rp.4000    |");
        System.out.println("|===================================|\n");

        while (true) {
            System.out.print("Pilih produk (1-" + buah.length + "), 0 untuk selesai: ");
            int pilihan = input.nextInt();
            if (pilihan == 0) {
                break;
            } else if (pilihan >= 1 && pilihan <= buah.length) {
                System.out.print("Jumlah yang di beli : ");
                int diBeli = input.nextInt();
                if (diBeli >= 1) {
                    int index = pilihan - 1;
                    jumlah[index] += diBeli;
                    jumlahTotal += harga[index] * diBeli;
                    System.out.println(buah[index] + " ditambahkan ke keranjang.");
                } else {
                    System.out.println("Jumlah yang dimasukkan salah.");
                }
            } else {
                System.out.println("Pilihan anda salah. Silakan pilih produk yang benar.");
            }
        }
        System.out.println("Rincian Pembelian :");
        for (int i = 0; i < buah.length; i++) {
            if (jumlah[i] > 0) {
                System.out.println(buah[i] + "   " + jumlah[i] + " pcs   Rp." + (harga[i] * jumlah[i]));

            }
        }
        if (jumlahTotal > 100000) {
            System.out.println("anda mendapat diskon 10%");
            int diskon = jumlahTotal * 10 / 100;
            jumlahTotal = jumlahTotal - diskon;
        }
        System.out.println("Total Pembayaran : Rp." + jumlahTotal);
        System.out.print("Jumlah uang yang diberikan anda : Rp.");
        int Pembayaran = input.nextInt();
        input.close();
        if (Pembayaran >= jumlahTotal) {
            int Kembalian = Pembayaran - jumlahTotal;
            System.out.println("Kembalian anda : Rp." + Kembalian);
        } else {
            System.out.println("Uang yang diberikan anda kurang. Pembayaran tidak berhasil.");
        }
        System.out.println("Terima kasih sudah berbelanja di toko kami!");
    }
}
