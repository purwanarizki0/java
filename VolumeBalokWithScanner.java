/*
 * Nama : Purwana Rijki Pirmansah
 * Kelas : 1A
 * 23 oktober 2023
 */

// menambahkan Scanner
import java.util.Scanner;

public class VolumeBalokWithScanner {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // mendefinisikan variable panjang, dan menginputkan panjang balok
        System.out.print("Masukkan panjang balok: ");
        int panjang = input.nextInt();

        // mendefinisikan variabel lebar, dan menginputkan lebar balok
        System.out.print("Masukkan lebar balok: ");
        int lebar = input.nextInt();

        // mendefinisikan variabel tinggi, dan menginputkan tinggi balok
        System.out.print("Masukkan tinggi balok: ");
        int tinggi = input.nextInt();

        // Menghitung volume balok
        int volume = panjang * lebar * tinggi;

        // Menampilkan hasil
        System.out.println("Volume Balok = " + volume);

        // Menutup Scanner
        input.close();
    }
}